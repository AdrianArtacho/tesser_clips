This Patch is part of the [TESSER environment](https://bitbucket.org/AdrianArtacho/tesserakt/src/master/).

![TTESS:Logo](https://bitbucket.org/AdrianArtacho/tesserakt/raw/HEAD/TESSER_logo.png)

# Tesser_clips

This deviceallows to launch clips via *midinotes* or a CC number of your choice.

### ![repo:TESS:clips](https://docs.google.com/drawings/d/e/2PACX-1vSkkHoAN80Og29fcneGOyJ7erULV0l2dz4fQ-cN6xAOk0nZGHBX5hnh4OrLknpfxlRtCZNM74UiIJA-/pub?w=444&h=390)

### Usage

Remember to place the clips that you wish to trigger at the leftmost of the live set, perhaps grouped, because the launcher has some trouble when groups are un/collapsed.

### Credits

patch largely based on [Connor Pogue's](https://connorpogue.gumroad.com/?recommended_by=library) M4L device ["M4L Clip Launcher"](https://app.gumroad.com/d/6e3e98de0c297cac2a48de0a43a90494).

____

# To-Do

- Added a "banks" analogy, letting the *column* (Track in live) be set via CC

- Make the "Clip Slot" ignore click, so that it is clear which numboxes are usable...